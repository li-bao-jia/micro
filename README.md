<p align="center">Node Grpc</p>
<p align="center">Node Grpc 微服务服务端案例代码</p>


### 安装

- 加载依赖
    ```sh
    npm install
    ```

### 使用

- 启动服务端
    ```sh
    node index.js 
    ```

- 启动客户端
    ```sh
    node client.js 
    ```


### 备注

- 本来打算直接直接写完逻辑，用php调用微服务，完成小程序发布功能，
结果发现PHP还需要安装 gRPC 和 Protobuf 扩展，才可以用grpc方式
通信，不想安装扩展，想要逻辑更简单些

- 后面打算用 HTTP 和 JSON 方式来实现，这个代码就留着备用吧，改一
改，可以做其他node grpc微服务起始代码
