// 1、引入proto文件
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

// 2、加载proto文件
const packageDefinition = protoLoader.loadSync('./protos/applet.proto', {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});
const appletProto = grpc.loadPackageDefinition(packageDefinition).applet;

// 3、建立链接
const client = new appletProto.Greeter('localhost:3000', grpc.credentials.createInsecure());

// 4、封装方法
function callUpload(name) {
    client.Upload({ name: name }, (err, response) => {
        if (err) console.error(err);
        else console.log('Upload:', response.message);
    });
}

function callPreview(name) {
    client.Preview({ name: name }, (err, response) => {
        if (err) console.error(err);
        else console.log('Preview:', response.message);
    });
}

// 5、执行调用
callUpload();
