// 1、引入proto文件
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

// 2、加载proto文件
const packageDefinition = protoLoader.loadSync('./protos/applet.proto', {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});
const appletProto = grpc.loadPackageDefinition(packageDefinition).applet;

// 3、启动服务
const index = new grpc.Server();

// 4、注册方法
index.addService(appletProto.Greeter.service, {
    Upload: (call, callback) => {
        callback(null, {message: 'Upload123 ' + call.request.name});
    },
    Preview: (call, callback) => {
        callback(null, {message: 'Preview123 ' + call.request.name});
    }
});

// 5、监听端口
index.bindAsync('0.0.0.0:3000', grpc.ServerCredentials.createInsecure(), () => {
    console.log('Server running at http://0.0.0.0:3000');
});
